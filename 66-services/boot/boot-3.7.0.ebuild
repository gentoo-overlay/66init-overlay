# Copyright 1999-2024 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="complete and portable set of services to properly boot a machine running on 66"
HOMEPAGE="https://git.obarun.org/66-service/arch/boot"
SRC_URI="https://git.obarun.org/66-service/arch/${PN}/-/archive/${PV}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~x86"
#IUSE="btrfs doc doc-html dmraid mdraid iptables lvm man nftables zfs"

RDEPEND="
	sys-apps/66
	sys-apps/66-tools
	sys-apps/s6-linux-utils
	sys-apps/s6-portable-utils
"

#PATCHES=(
#   "${FILESDIR}/replace-s6test-with-test.patch"
#)

#pkg_setup() {
#   use doc-html && HTML_DOCS=( doc/html/. )
#}

src_configure() {
	tc-export AR CC RANLIB

	local mydate=$(date +%a)
	local myhost="$mydate$RANDOM"

	local myconf=(
		--bindir=/bin
		--libdir="/usr/$(get_libdir)/${PN}"
		--HOSTNAME="$myhost"
    )

#		--with-system-seed="/usr/share/66/seed"
#		--with-system-script="/usr/share/66/script"
#		--with-system-service="/usr/share/66/service"

    econf "${myconf[@]}"
}

src_compile() {
   emake DESTDIR="${D}"
}

src_install() {
   emake DESTDIR="${D}" install

#   find "${ED}/usr/share" -type d -name "doc" | xargs rm -r
#   find "${ED}/usr/share" -type d -name "man" | xargs rm -r

#   use doc && dodoc AUTHOR*
#   use man && doman doc/man/man*/*.1

#   if use doc-html ; then
#      docinto html
#      dodoc doc/html/boot@.html
#   fi
}
