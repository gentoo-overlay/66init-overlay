# Copyright 1999-2024 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="66 tools built around s6 programs"
HOMEPAGE="https://git.obarun.org/Obarun/66/"
SRC_URI="https://git.obarun.org/Obarun/${PN}/-/archive/${PV}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~x86"
#IUSE="doc doc-html man sysinit"

RDEPEND="
	!sys-apps/sysvinit
	acct-user/s6log
	acct-group/s6log
	dev-lang/execline:=
	>=dev-libs/skalibs-2.14.1.1:=
	>=sys-apps/s6-2.12.0.3:=[execline]
	dev-libs/oblibs:=
"
DEPEND="${RDEPEND}
	app-text/lowdown
"

#pkg_setup() {
#    use doc-html && HTML_DOCS=( doc/html/. )
#}

src_prepare() {
	default

	# Avoid QA warning for LDFLAGS addition
	sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

	sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die

	# 66-boot option for Gentoo
	sed -i 's/66-boot/66-boot -m \/run/' skel/init
}

#Avoid unexpected path for html documentation, let einstalldocs do the job
#sed -i '/^install:/ s/install-html //' Makefile

src_configure() {
	tc-export AR CC RANLIB

	local myconf=(
		--bindir=/bin
		--dynlibdir="/$(get_libdir)"
		--libdir="/usr/$(get_libdir)/${PN}"
		--libexecdir=/lib/s6
		--with-dynlib="/$(get_libdir)"
		--with-lib="/usr/$(get_libdir)/execline"
		--with-lib="/usr/$(get_libdir)/s6"
		--with-lib="/usr/$(get_libdir)/oblibs"
		--with-lib="/usr/$(get_libdir)/skalibs"
		--with-sysdeps="/usr/$(get_libdir)/skalibs"
		--with-default-tree-name=main
		--with-s6-log-user=s6log
		--enable-shared
		--disable-allstatic
		--disable-static
		--disable-static-libc
	)

	econf "${myconf[@]}"
}

#src_compile() {
#    if use doc-html ; then
#            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
#                ./doc/make-html.sh || die
#            popd > /dev/null   || die
#    fi

#    if use man ; then
#            pushd "${WORKDIR}/${PN}-v${PV}" > /dev/null || die
#               ./doc/make-man.sh  || die
#            popd > /dev/null   || die
#    fi
#}

#src_install() {
#mkdir "${D}/sbin"
#mkdir "${D}/usr/bin"

#mv "${D}/etc/66/init"       "${D}/sbin/"
#mv "${D}/etc/66/halt"       "${D}/usr/bin/"
#mv "${D}/etc/66/poweroff"   "${D}/usr/bin/"
#mv "${D}/etc/66/reboot"     "${D}/usr/bin/"
#mv "${D}/etc/66/shutdown"   "${D}/usr/bin/"
#}

#    emake DESTDIR="${D}" install

#    use doc-html && einstalldocs

#    if ! use doc && use doc-html ; then
#        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
#        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
#    fi

#    if use doc && ! use doc-html ; then
#        dodoc AUTHOR* README*
#    fi

#    use man && doman doc/man/man*/*.[158]


#    if use sysinit ; then

#    fi
#}
