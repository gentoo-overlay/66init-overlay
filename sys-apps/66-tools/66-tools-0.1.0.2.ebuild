# Copyright 1999-2024 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit toolchain-funcs

DESCRIPTION="Small tools and helpers for service scripts execution"
HOMEPAGE="https://git.obarun.org/Obarun/66-tools/"
SRC_URI="https://git.obarun.org/Obarun/${PN}/-/archive/${PV}/${P}.tar.gz"

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~x86"
#IUSE="doc doc-html man"

RDEPEND="
	dev-lang/execline:=
	>=dev-libs/skalibs-2.14.1.1:=
	dev-libs/oblibs:=
"
DEPEND="${RDEPEND}
	app-text/lowdown
"

#pkg_setup() {
#    use doc-html && HTML_DOCS=( doc/html/. )
#}

src_prepare() {
	default

	# Avoid QA warning for LDFLAGS addition
	sed -i -e 's/.*-Wl,--hash-style=both$/:/' configure || die

	sed -i -e '/AR := /d' -e '/RANLIB := /d' Makefile || die
}

#Avoid unexpected path for html documentation, let einstalldocs do the job
#sed -i '/^install:/ s/install-html //' Makefile

src_configure() {
	tc-export AR CC RANLIB

	local myconf=(
		--bindir=/bin
		--dynlibdir="/$(get_libdir)"
		--libdir="/usr/$(get_libdir)/${PN}"
		--libexecdir=/lib/s6
		--with-dynlib="/$(get_libdir)"
		--with-lib="/usr/$(get_libdir)/execline"
		--with-lib="/usr/$(get_libdir)/skalibs"
		--with-lib="/usr/$(get_libdir)/oblibs"
		--with-sysdeps="/usr/$(get_libdir)/skalibs"
		--with-ns-rule=/usr/share/66/script/ns
		--enable-shared
		--disable-allstatic
		--disable-static
		--disable-static-libc
	)

	econf "${myconf[@]}"
}

#src_install() {
#    emake DESTDIR="${D}" install install-ns-rule

#    use doc-html && einstalldocs

#    if ! use doc && use doc-html ; then
#        find "${ED}/usr/share/doc" -iname "AUTHOR*" | xargs rm
#        find "${ED}/usr/share/doc" -iname "README*" | xargs rm
#    fi

#    if use doc && ! use doc-html ; then
#        dodoc AUTHOR* README*
#    fi

#    use man && doman doc/man/man*/*.1
#}
