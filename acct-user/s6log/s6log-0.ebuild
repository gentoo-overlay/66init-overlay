# Copyright 2019-2024 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

ACCT_USER_ID=23
ACCT_USER_HOME=/var/log/66
ACCT_USER_GROUPS=( s6log )

acct-user_add_deps
